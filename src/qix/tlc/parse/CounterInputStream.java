package qix.tlc.parse;

import java.io.IOException;
import java.io.InputStream;

/**
 * InputStream adapter class (arguably a wrapper class)
 * that silently monitors the lines/columns counts in a stream.
 * 
 * @author Qix
 *
 */
public class CounterInputStream extends InputStream {

	private final InputStream stream;
	private char last = (char) -1;
	private int line = 1;
	private int col = 1;
	
	public CounterInputStream(InputStream stream)
	{
		// Store
		this.stream = stream;
	}
	
	@Override
	public int read() throws IOException {
		// Get next
		int readInt = this.stream.read();
		
		// Convert to character
		char cha = (char) readInt;
		
		// Is it a new line?
		if(cha == '\r' || (cha == '\n' && last != '\r'))
		{
			// Inc + Reset
			line++;
			col = 1;
		}else if(!(cha == '\n' && last == '\r') && readInt != -1)
			// Inc
			col++;
		
		// Store last
		last = cha;
		
		// Return next
		return readInt;
	}

	/**
	 * @return The current line the reader is on
	 */
	public int getLine()
	{
		// Return
		return this.line;
	}
	
	/**
	 * @return The current column the reader is on
	 */
	public int getCol()
	{
		// Return
		return this.col;
	}
}
