package qix.tlc.parse;

/**
 * Specializes/simplifies an internal
 * {@link StringBuffer} to accommodate
 * for the common operations needed by TLC's
 * parsing system
 * 
 * @author Qix
 *
 */
public class SimpleStringBuffer {
	
	private final StringBuffer buffer = new StringBuffer();
	
	public SimpleStringBuffer append(char cha)
	{
		// Proxy + Return
		this.buffer.append(cha);
		return this;
	}
	
	public SimpleStringBuffer append(String str)
	{
		// Proxy + Return
		this.buffer.append(str);
		return this;
	}
	
	public SimpleStringBuffer erase()
	{
		// Forward + Return
		return this.erase(1);
	}
	
	public SimpleStringBuffer erase(int count)
	{
		// 0?
		if(count == 0)
			return this;
		
		// Delete + Return
		int len = this.buffer.length();
		this.buffer.delete(len - count, len);
		return this;
	}
	
	@Override
	public String toString() {
		// Return
		return this.buffer.toString();
	}
}
