package qix.tlc.parse;

import java.util.Arrays;
import java.util.List;

/**
 * Burns a line
 * 
 * @author Qix
 *
 */
class LineBurner extends SimpleBurner {

	private static final List<Character> LINE_CHARS =
			Arrays.asList('\n', '\r', '\f');
	
	LineBurner(boolean processCurrent)
	{
		// Super
		super(processCurrent);
	}
	
	@Override
	BurnAction getAction(char cha) {
		// Is it an LF/CR?
		if(LineBurner.LINE_CHARS.contains(cha))
			return BurnAction.ABORT;
		
		// Continue
		return BurnAction.CONTINUE;
	}

}
