package qix.tlc.parse;

/**
 * Thrown when a parsing exception
 * occurs
 * 
 * @author Qix
 *
 */
public class ParseException extends Exception {

	private static final long serialVersionUID = -2392803449346908185L;

	public ParseException(Throwable t, int line, String fmt, Object... args)
	{
		// This
		this(t, line, 1, fmt, args);
	}
	
	public ParseException(int line, String fmt, Object... args)
	{
		// This
		this(line, 1, fmt, args);
	}
	
	public ParseException(Throwable t, int line, int col, String fmt, Object... args)
	{
		// Super
		super(String.format(
				fmt + String.format(" <line %d, col %d>", line, col), args), t);
	}
	
	public ParseException(int line, int col, String fmt, Object... args)
	{
		// Super
		super(String.format(
				fmt + String.format(" <line %d, col %d>", line, col), args));
	}
	
	public ParseException(Throwable t, String fmt, Object... args)
	{
		// Super
		super(String.format(fmt, args), t);
	}
	
	public ParseException(String fmt, Object... args)
	{
		// Super
		super(String.format(fmt, args));
	}
}
