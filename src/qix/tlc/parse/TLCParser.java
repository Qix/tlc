package qix.tlc.parse;

import java.io.IOException;
import java.io.InputStream;

import qix.tlc.parse.IBurner.BurnResult;

/**
 * Handles the parsing of a C/C++ file
 * 
 * I pursued several avenues to parse these
 * files, including JavaCC, Yacc (ew), and
 * even considering building a JNI binding
 * and hack-wrapper for CLang.
 * 
 * This was the most least-ridiculous way
 * to do this.
 * 
 * The way this class works:
 * - 1. Burn to a delimiter
 * - 2. Test that it's not a syntax error/we're in a string
 * - 3. Begin a scope stack iterator
 * - 4. Once back to parent scope, send to visitor
 * 
 * @author Qix
 *
 */
public class TLCParser {
	
	static final int LOOKBEHIND = 4;
	
	private final CounterInputStream stream;
	private final ITLCVisitor visitor; 
	private final char[] buf = new char[TLCParser.LOOKBEHIND];
	private final SimpleStringBuffer finalBuffer = new SimpleStringBuffer();
	
	private char residual = (char) -1;
	
	public TLCParser(InputStream stream, ITLCVisitor visitor) throws ParseException, IOException
	{
		// Store
		this.stream = new CounterInputStream(stream);
		this.visitor = visitor;
		
		// Initialize buffer
		this.resetBuffer();
		
		// Process
		this.process();
	}
	
	private void process() throws IOException, ParseException
	{
		// Start processing
		for(;;)
		{
			// Burn to delimiter
			Delimiter delim = null;
			if((delim = this.burnToDelim()) == null)
				return;
			
			// Get burned block
			BurnResult burnResult = delim.burner.burn(this.stream, this.get());
			
			// Backup
			if(delim.backup > 0)
				// Erase
				this.finalBuffer.erase(delim.backup);
			
			// Check for anomalies
			if(!(burnResult == null || burnResult.burnData.length() == 0))
			{
				// Visit
				String visitedData = delim.proxy.visit(
						this.visitor,
						burnResult.burnData,
						this.stream.getLine(),
						this.stream.getCol()
						);
				
				// Add to buffer
				if(visitedData != null)
					this.finalBuffer.append(visitedData);
				
				// Process residual if necessary
				if(burnResult.residual != (char)-1)
					// Set residual
					this.residual = burnResult.residual;
			}
			
			// Reset buffer
			this.resetBuffer();
		}
	}
	
	private void resetBuffer()
	{
		// Iterate
		int len = this.buf.length;
		for(int i = 0; i < len; i++)
			this.buf[i] = (char) -1;
	}
	
	private Delimiter burnToDelim() throws IOException
	{
		// Burn loop
		while(this.burnSingle())
		{
			// Get/check delim
			Delimiter delim = Delimiter.getByChar(this.get(), this.get(1));
			if(delim == null) continue;
			
			// Check previous block
			if(delim.comparator != null && !delim.comparator.matches(this.buf))
				// This isn't the droid we're looking for!
				continue;
			
			// Backup 2
			this.finalBuffer.erase(2);
			
			// Return
			return delim;
		}
	
		// Return
		return null;
	}
	
	/**
	 * @return The latest character read
	 */
	private char get()
	{
		// Return
		return this.get(0);
	}
	
	/**
	 * Gets one of the last characters read
	 * given an offset (0 being latest, 1 being
	 * second latest, etc.)
	 * @param backOffset The offset to retrieve
	 * @return The character that was read, or -1 if we haven't read that far.
	 */
	private char get(int backOffset)
	{
		// Out of bounds?
		if(backOffset >= TLCParser.LOOKBEHIND)
			throw new RuntimeException(
					String.format("Back offset larger than lookbehind buffer: %d >= %d",
							backOffset,
							TLCParser.LOOKBEHIND));
		
		// Get
		return this.buf[TLCParser.LOOKBEHIND - ++backOffset];
	}
	
	/**
	 * Burns the next character, shifting the buffer.
	 * @return True if there was another character, false if end of stream was reached
	 * @throws IOException Thrown when the file could not be read.
	 */
	private boolean burnSingle() throws IOException
	{
		// Shift buffer
		this.shiftBuffer();
		
		// Read another character
		if(this.residual != (char) -1)
		{
			this.buf[TLCParser.LOOKBEHIND - 1] = this.residual;
			this.residual = (char)-1;
		}else
			this.buf[TLCParser.LOOKBEHIND - 1] = (char)this.stream.read();
		
		// Append to final buffer + Return true
		if(this.get() != (char)-1)
		{
			this.finalBuffer.append(this.get());
			return true;
		}
		
		// Return
		return false;
	}
	
	/**
	 * Shifts the buffer forward one step
	 */
	private void shiftBuffer()
	{
		// Iterate
		for(int i = 0; i < TLCParser.LOOKBEHIND - 1; i++)
			buf[i] = buf[i+1];
	}
	
	@Override
	public String toString() {
		// Return
		return this.finalBuffer.toString();
	}
}
