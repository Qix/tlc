package qix.tlc.parse;

import java.io.IOException;
import java.io.InputStream;

/**
 * Defines methods for a class that
 * accepts a stream, burns it and returns
 * some arbitrary data, setting it up
 * for continued burns.
 * 
 * @author Qix
 *
 */
interface IBurner {
	/**
	 * Burns the stream, returning an arbitrary block
	 * of that stream.
	 * @param stream The stream to burn
	 * @param currentCharacter The current top-level character
	 * @return The block of information to return along with an optional residual character
	 * @throws IOException Thrown when something goes wrong in the reading of the stream
	 * @throws ParseException Thrown when something goes wrong while parsing
	 */
	public BurnResult burn(InputStream stream, char currentCharacter) throws IOException, ParseException;
	
	static class BurnResult
	{
		public final String burnData;
		public final char residual;
		
		public BurnResult(String burnData)
		{
			// This
			this(burnData, (char) -1);
		}
		
		public BurnResult(String burnData, char residual)
		{
			// Store
			this.burnData = burnData;
			this.residual = residual;
		}
	}
}
