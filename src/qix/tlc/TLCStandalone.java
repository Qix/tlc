package qix.tlc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import qix.tlc.args.ArgumentVisitor;
import qix.tlc.parse.ParseException;

/**
 * TLC, the Tiny Lua-equipped C-preprocessor
 *  by Qix, github.com/Qix-/TLC
 *  
 *  
 * 
 * @author Qix
 *
 */
public class TLCStandalone {
	
	public static final String VERSION_STRING = "v0.0.1a (Alpha)";
	
	private static final HashSet<String> ADDITIONAL_PATHS = new HashSet<>();
	private static final HashSet<String> AUTOMATIC_IMPORTS = new HashSet<>();
	private static final Path WORKING_DIR = Paths.get(System.getProperty("user.dir"));
	
	/**
	 * TLC Main entry point
	 * @param rawArgs Arguments inputed from the command line
	 */
	public static void main(String[] rawArgs)
	{		
		// Parse arguments
		TLCArguments args = new TLCArguments();
		ArgumentVisitor visitor = new ArgumentVisitor(args);
		
		// Visit
		try
		{
			visitor.visit(rawArgs);
		}catch(IllegalArgumentException e)
		{
			// Show and return
			TLCStandalone.quit(e.getMessage());
		}
		
		// Version message?
		if(args.isVersionRequest())
		{
			// Display and return
			System.out.println(TLCStandalone.getVersionString(false));
			return;
		}
		
		// Help message?
		if(args.isHelpRequest())
		{
			// Display and return
			System.out.println(visitor);
			return;
		}
		
		// Check file count
		if(args.getFiles().size() == 0)
			TLCStandalone.quit("No input files");
		else if(args.getFiles().size() > 1 && args.isDependencyList())
			TLCStandalone.quit("Dependency mode (-D) can only be used with one file at a time.");
		
		// Iterate files
		for(File file : args.getFiles())
			// Process
			TLCStandalone.process(file, args);
	}
	
	static void quit(String error)
	{
		// Forward
		TLCStandalone.quit(error, 1);
	}
	
	static void quit(String error, int code)
	{
		// Echo
		System.err.println(error);
		
		// Quit
		System.exit(code);
	}
	
	/**
	 * Processes a single file
	 * @param file The file to process
	 * @param arguments The arguments to use while processing
	 */
	private static void process(File file, TLCArguments arguments)
	{
		// Select output file
		File outputFile = null;
		if(file.isAbsolute())
		{
			// Absolute paths are relativized against the
			//	current directory and then prefixed.
			try {
				outputFile = Paths.get(
						arguments.getPrefix().toString(),
						TLCStandalone.WORKING_DIR.relativize(
								file.getCanonicalFile().toPath())
									.toString()
							).toFile();
			} catch (IOException e) {
				// Error
				System.err.format("WARNING: input file could not canonicalized: %s%n",
						file.toString());
				return;
			}
		}else{
			// Preserve path?
			if(arguments.isPathPreserved())
			{
				// Get rid of initial ../'s
				Path path = file.toPath();
				while(path.getNameCount() > 1 && path.getName(0).toString().equals(".."))
					path = path.subpath(1, path.getNameCount());
				
				// Prepend prefix and set output file
				outputFile = Paths.get(
						arguments.getPrefix().toString(),
						path.toString()
							).toFile();
			}else{
				// Get filename and prepend prefix
				outputFile = Paths.get(
						arguments.getPrefix().toString(),
						file.getName()
							).toFile();
			}
		}
	
		// Remove any TLC ending
		outputFile = new File(
				outputFile
					.toString()
					.replaceAll("\\.[Tt][Ll][Cc]$", ""));
	
		// Only warn/create parent dirs
		//	if we're not listing dependencies
		//	and if we're not in test mode
		if(!arguments.isTestMode() && !arguments.isDependencyList())
		{
			// Identical?
			if(outputFile.getAbsoluteFile().equals(file.getAbsoluteFile()) &&
					!arguments.isForcedOverwrite())
			{
				// Warn and return
				System.err.format("WARNING: input location is identical to output location (use -f to force overwrite): %s%n",
						file);
				return;
			}
		
			// Non-existant directory?
			if(	!outputFile.getParentFile().exists() ||
				!outputFile.getParentFile().isDirectory())
			{
				if(arguments.isParentPathCreated())
				{
					// Make
					if(!outputFile.getParentFile().mkdirs())
					{
						// Warn and return
						System.err.format("WARNING: could not make parent directories (unknown error): %s%n",
								outputFile);
						return;
					}
				}else{
					// Warn and return
					System.err.format("WARNING: output directory does not exist (use -p to auto-create): %s%n",
							outputFile);
					return;
				}
			}
		}
		
		// Echo
		if(arguments.isVerbose())
		{
			// Format output path
			//	Hnnngg
			String outputPath = outputFile
					.toString()
					.replace("\\.\\", "\\")
					.replace("/./", "/");
			
			// Format
			if(arguments.isBareFormat())
				System.out.println(outputPath);
			else
				System.out.format("%s -> %s%n",
						file,
						outputPath);
		}
		
		// In test mode?
		if(!arguments.isTestMode())
		{
			try {
				// Process file
				TLCProcessor processor = new TLCProcessor(
						file,
						arguments.isDependencyList());
				
				// Dependency list?
				if(arguments.isDependencyList())
				{
					// Get base dependency path
					Path relativization = TLCStandalone.WORKING_DIR;
					if(arguments.getDependsPrefix() != null)
						relativization = arguments.getDependsPrefix();
					
					// Output
					for(File f : processor.getDependencies())
						System.out.println(
								relativization.relativize(f.getCanonicalFile().toPath()) +
								(!f.exists() ? " (?)" : "")
								);
				}else{
					// Create file, if it doesn't exist
					if(!outputFile.exists() && !outputFile.createNewFile())
					{
						// Warn and return
						System.err.format("WARNING: Could not create output file; skipping: %s%n",
								outputFile);
						return;
					}
					
					// Output File
					try(FileWriter fw = new FileWriter(outputFile, false))
					{
						// Write
						fw.append(processor.toString());
					}
				}
			} catch (ParseException e) {
				// Display + Return
				System.err.println(e.getMessage());
				return;
			} catch (IOException e) {
				// Re-throw
				throw new RuntimeException(e);
			}
		}
	}
	
	/**
	 * Adds a path that is automatically added to each Lua
	 * state upon creation
	 * @param path The path to add
	 */
	public static void addAdditionalPath(String path)
	{
		// Add
		TLCStandalone.ADDITIONAL_PATHS.add(path);
	}
	
	/**
	 * @return All additional paths that are added to Lua states
	 */
	public static Set<String> getAdditionalPaths()
	{
		// Return
		return Collections.unmodifiableSet(TLCStandalone.ADDITIONAL_PATHS);
	}
	
	/**
	 * Adds a string that is passed to the import method on
	 * new lua states upon creation
	 * @param path The import string to add
	 */
	public static void addAutomaticImport(String path)
	{
		// Add
		TLCStandalone.AUTOMATIC_IMPORTS.add(path);
	}
	
	/**
	 * @return All strings automatically imported to new lua states
	 */
	public static Set<String> getAutomaticImports()
	{
		// Return
		return Collections.unmodifiableSet(TLCStandalone.AUTOMATIC_IMPORTS);
	}
	
	/**
	 * @param author Whether or not to show the author
	 * @return A version string with author and title and such
	 */
	public static String getVersionString(boolean author)
	{
		// Return
		return String.format(
				"Tiny Lua-Equipped C-Preprocessor (TLC) <%s>%s",
				TLCStandalone.VERSION_STRING,
				author ? String.format("%n  created by Qix <i.am.qix@gmail.com>; github.com/Qix-/TLC") : ""
				);
	}
}