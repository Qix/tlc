package qix.tlc.args;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Denotes a Flag (-[a-zA-Z]) that can be
 * combined with other flags (for clusters that
 * match -[a-zA-Z]+)
 * 
 * @author Qix
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Flag {
	/**
	 * @return The character that triggers this flag
	 */
	public char value();
	
	/**
	 * @return The help message for this flag entry
	 */
	public String message();
}
