package qix.tlc.args;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Denotes a method that handles
 * an option (--option[=["]value["]])
 * 
 * @author Qix
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Option {
	
	/**
	 * Lists the possible argument requirements
	 * for an option
	 * 
	 * @author Qix
	 *
	 */
	public enum ArgumentRequirement
	{
		/**
		 * Argument MUST be present
		 * Callback must have a String argument
		 */
		REQUIRED,
		
		/**
		 * Argument MAY be present
		 * Callback must have a String argument and must
		 * check it for null
		 */
		OPTIONAL,
		
		/**
		 * Argument MUST NOT be present
		 * Callback must have no arguments
		 */
		ABSENT,
	}
	
	/**
	 * @return The name of this option (without -- prefix)
	 */
	public String value();
	
	/**
	 * @return The argument requirement of the option
	 */
	public ArgumentRequirement argument()
		default ArgumentRequirement.ABSENT;
	
	/**
	 * @return If true, the option can only be specified once
	 */
	public boolean unique()
		default true;
	
	/**
	 * @return The help message for this option entry
	 */
	public String message();
}
