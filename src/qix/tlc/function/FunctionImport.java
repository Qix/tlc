package qix.tlc.function;

import java.util.List;

import qix.tlc.TLCProcessor;
import qix.tlc.parse.ParseException;

/**
 * Function to import a file from
 * an include path (using `require`)
 * 
 * @author Qix
 *
 */
public class FunctionImport implements IFunction {

	@Override
	public void checkArgs(TLCProcessor processor, List<String> args, int line) throws ParseException {
		// Check arg length
		if(args.size() != 1)
			// Throw
			throw new ParseException(line, "Import takes 1 argument; %d given", args.size());
	}

	@Override
	public String doFunction(TLCProcessor processor, List<String> args, int line)
			throws ParseException {
		// Require + Return
		processor.getLua().importLua(args.get(0));
		return null;
	}

}
