package qix.tlc.function;

import java.util.List;

import qix.tlc.TLCProcessor;
import qix.tlc.parse.ParseException;

/**
 * List of all functions
 * 
 * @author Qix
 *
 */
public enum Function implements IFunction {
	IMPORT(new FunctionImport()),
	;
	
	private final IFunction handler;
	Function(IFunction handler)
	{
		// Store
		this.handler = handler;
	}
	@Override
	public void checkArgs(TLCProcessor processor, List<String> args, int line) throws ParseException {
		// Proxy
		this.handler.checkArgs(processor, args, line);
	}
	@Override
	public String doFunction(TLCProcessor processor, List<String> args, int line) throws ParseException {
		// Proxy
		return this.handler.doFunction(processor, args, line);
	}
	
}
