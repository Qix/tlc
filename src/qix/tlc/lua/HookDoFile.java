package qix.tlc.lua;

import java.io.File;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Hooks dofile() to allow the recording
 * of dependencies
 * 
 * @author Qix
 *
 */
class HookDoFile extends OneArgFunction {
	
	private final LuaValue original;
	private final LuaListener callback;
	
	protected HookDoFile(LuaValue original, LuaListener callback)
	{
		// Store
		this.original = original;
		this.callback = callback;
	}
	
	@Override
	public LuaValue call(LuaValue arg) {
		// Check for nil
		if(arg.isnil())
			return LuaValue.NIL;
		
		// Transform path
		String transformed = this.callback.transformLuaImport(arg.tojstring());
		
		// Call original
		LuaValue result = this.original.call(transformed);
		
		// Callback
		this.callback.onLuaDependency(
				new File(transformed));
		
		// Return
		return result;
	}

	/**
	 * Hooks dofile() in a globals object
	 * @param globals The globals object for which to hook dofile()
	 * @param callback The callback to alert when a dependency is loaded
	 */
	public static void hook(Globals globals, LuaListener callback)
	{
		// Get original
		LuaValue original = globals.get("dofile");
		
		// Create
		HookDoFile doFile = new HookDoFile(original, callback);
		
		// Hook
		globals.set("dofile", doFile);
	}
}
