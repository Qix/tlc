package qix.tlc.lua;

import java.io.File;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Hooks the require() function and performs
 * the lookup/calling itself using dofile().
 * 
 * @author Qix
 *
 */
class HookRequire extends OneArgFunction {
	
	private final Globals globals;
	private final LuaListener callback;
	
	private HookRequire(Globals globals, LuaListener callback)
	{
		// Store
		this.globals = globals;
		this.callback = callback;
	}
	
	@Override
	public LuaValue call(LuaValue arg) {
		// Check arg
		arg.checknotnil();
		String argStr = arg.tojstring();
		
		// Get package path
		String packagePath = this.globals
				.get("package")
				.get("path")
				.tojstring()
				.replace("?", argStr);
		
		// Get dofile
		LuaValue dofile = this.globals.get("dofile");
		
		// Iterate
		for(String path : packagePath.split(";"))
		{
			// Adjust path
			String transPath = this.callback.transformLuaImport(path);
			
			// Get file
			File file = new File(transPath);
			
			// Exist?
			if(file.exists())
				// Do + Return
				return dofile.call(path);
		}
		
		// Throw error
		LuaValue.error(
			String.format(
					"Module '%s' not found: %s",
					argStr,
					argStr));
		
		// Return (Never hits)
		return LuaValue.NIL;
	}

	/**
	 * Hooks the require function
	 * @param globals The globals for which to hook
	 * @param callback The Lua listener to use to transform paths
	 */
	public static void hook(Globals globals, LuaListener callback)
	{
		// Create function
		HookRequire hook = new HookRequire(globals, callback);
		
		// Set
		globals.set("require", hook);
	}
}
