package qix.tlc.lua;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;

/**
 * Hooks loadfile() to allow the recording
 * of dependencies
 * 
 * @author Qix
 *
 */
class HookLoadFile extends HookDoFile {

	private HookLoadFile(LuaValue original, LuaListener callback)
	{
		// Super
		super(original, callback);
	}

	/**
	 * Hooks loadfile() in a globals object
	 * @param globals The globals object for which to hook loadfile()
	 * @param callback The callback to alert when a dependency is loaded
	 */
	public static void hook(Globals globals, LuaListener callback)
	{
		// Get original
		LuaValue original = globals.get("loadfile");
		
		// Create
		HookDoFile doFile = new HookDoFile(original, callback);
		
		// Hook
		globals.set("loadfile", doFile);
	}
}
