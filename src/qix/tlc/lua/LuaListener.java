package qix.tlc.lua;

import java.io.File;

/**
 * Defines a callback that is called by 
 * various Lua events. This is currently used
 * to calculate dependencies.
 * 
 * @author Qix
 *
 */
public interface LuaListener
{
	/**
	 * Called when a file is loaded into the state,
	 * either through dofile(), loadfile(), or require().
	 * @param file The file that was loaded
	 */
	public void onLuaDependency(File file);

	/**
	 * Used to transform the "working path" of a Lua file
	 * in order to correctly transform paths of dofile() and such.
	 * @return The path value to transform
	 */
	public String transformLuaImport(String path);
}
